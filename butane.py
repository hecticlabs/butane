#!/usr/bin/env python3

import serial
import time
import struct

DEVICE='/dev/tty.usbmodem2101'
FIRMWARE='unknown'
VERBOSE=True

def log(*args):
    print('[info]', *args)

def logv(*args):
    if VERBOSE:
        print('[verbose]', *args)

def loge(*args):
    print('[error]', *args)

def docmd(s, cmd):
    if cmd.startswith('/send '):
        log('butane: sending file...')
        with open(' '.join(cmd.split(' ')[1:]), 'rb') as f:
            d = f.read()
            l = len(d)
            s.write(struct.pack('<I', l))
            s.write(d)
        log('butane: file sent!')
    else:
        print('butane: unknown command')

# open a serial connection
log('connecting to bootctl mode device...')
while True:
    try:
        s = serial.Serial(DEVICE, 115200, timeout=1)
        break
    except Exception as e:
        s = str(e)
        if 'no such file or directory' not in s.lower():
            print(e)
        time.sleep(1)

log('serial device found!')

while True:
    logv('sending board request...')
    s.write(b'board\x00')
    s.flush()
    time.sleep(1)
    logv('reading board response...')
    bstr = s.read_until(b'\x00')
    if len(bstr) > 2:
        dat = bstr.decode()
        print(dat)
        if bstr.startswith(b'err'):
            continue
        FIRMWARE = dat.split(' ')[0]
        if '\n' in FIRMWARE:
            loge('device not in bootctl mode!')
        log('bootctl mode device found!')
        break

s.flush()

def readser():
    resp = s.read_until(b'\x00')
    if len(resp) > 0:
        try:
            print(resp.decode())
        except UnicodeDecodeError:
            print(resp)

try:
    while True:
        cmd = input(f'{FIRMWARE}> ')
        if len(cmd) < 1:
            continue
        if cmd.startswith('/'):
            docmd(s, cmd)
            readser()
            continue
        s.write(cmd.encode() + b'\x00')
        s.flush()
        readser()
except KeyboardInterrupt:
    pass
